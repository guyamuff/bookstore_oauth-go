package oauth

import (
	"encoding/json"
	"github.com/mercadolibre/golang-restclient/rest"
	"gitlab.com/guyamuff/bookstore_oauth-go/src/errors"
	"net/http"
	"time"
)

const (
	PRIVATE = "X-Private"
)

type OAuth interface {
	IsPrivate(*http.Request) bool
	GetAuth(*http.Request) (*AccessToken, *errors.RestErr)
}

type oAuth struct {
}

func NewOAuth() OAuth {
	return &oAuth{}
}

func (o oAuth) IsPrivate(r *http.Request) bool {
	pr := r.Header.Get(PRIVATE)
	return pr == "true"
}

func (o *oAuth) GetAuth(r *http.Request) (*AccessToken, *errors.RestErr) {

	tokenId := r.URL.Query().Get("access_token")
	if tokenId == "" {
		return nil, errors.NewBadRequestError("access_token not specified")
	}

	authServer := rest.RequestBuilder{
		BaseURL: "http://localhost:8081",
		Timeout: 100 * time.Millisecond,
	}
	resp := authServer.Get("/access_token/" + tokenId)
	if resp == nil || resp.Response == nil {
		return nil, errors.NewInternalError("problems getting token " + tokenId)
	}

	if resp.StatusCode > 299 {
		var restErr errors.RestErr
		err := json.Unmarshal(resp.Bytes(), &restErr)
		if err == nil {
			return nil, errors.NewNotFoundError("token not found " + tokenId)
		} else {
			return nil, errors.NewInternalError(err.Error())
		}
	}

	var ret AccessToken
	err := json.Unmarshal(resp.Bytes(), &ret)
	if err != nil {
		return nil, errors.NewInternalError(err.Error())
	}
	return &ret, nil
}
