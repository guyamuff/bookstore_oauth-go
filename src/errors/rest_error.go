package errors

import (
	"crypto/rand"
	"fmt"
	"gitlab.com/guyamuff/bookstore_oauth-go/src/logger"
	"math/big"
	"net/http"
)

type RestErr struct {
	Message string `json:"message"`
	Status  int    `json:"status"`
	Error   string `json:"error"`
}

func NewBadRequestError(message string) *RestErr {
	return &RestErr{
		Message: message,
		Status:  http.StatusBadRequest,
		Error:   "bad_request",
	}
}

func NewNotFoundError(message string) *RestErr {
	return &RestErr{
		Message: message,
		Status:  http.StatusNotFound,
	}
}

func NewInternalError(message string) *RestErr {
	var r *big.Int
	r, err := rand.Int(rand.Reader, big.NewInt(1000)) // Generate token to match external error to logged errors. Would
	// be better as an alphanumeric but this serves for now
	if err != nil {
		panic(err)
	}
	logger.Log.Error(fmt.Sprintf("Error %d: %s", r, message))
	return &RestErr{
		Message: fmt.Sprintf("Internal error %d", r),
		Status:  http.StatusInternalServerError,
	}
}

func NewAuthError() *RestErr {
	return &RestErr{
		Message: "Not authenticated",
		Status:  http.StatusUnauthorized,
	}
}
