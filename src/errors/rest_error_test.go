package errors

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"reflect"
	"testing"
)

func TestNewAuthError(t *testing.T) {
	tests := []struct {
		name string
		want *RestErr
	}{
		{name: "Not authorized",
			want: &RestErr{Status: http.StatusUnauthorized,
				Message: "Not authenticated"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewAuthError(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewAuthError() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewBadRequestError(t *testing.T) {
	type args struct {
		message string
	}
	tests := []struct {
		name string
		args args
		want *RestErr
	}{
		{name: "Bad request",
			args: args{message: "Foobar"},
			want: &RestErr{Status: http.StatusBadRequest,
				Error:   "bad_request",
				Message: "Foobar"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewBadRequestError(tt.args.message); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewBadRequestError() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewInternalError(t *testing.T) {
	restErr := NewInternalError("wahoo")
	assert.Equal(t, http.StatusInternalServerError, restErr.Status)
}

func TestNewNotFoundError(t *testing.T) {
	type args struct {
		message string
	}
	tests := []struct {
		name string
		args args
		want *RestErr
	}{
		{name: "404 not found",
			args: args{message: "yipes"},
			want: &RestErr{Status: http.StatusNotFound,
				Message: "yipes"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewNotFoundError(tt.args.message); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewNotFoundError() = %v, want %v", got, tt.want)
			}
		})
	}
}
