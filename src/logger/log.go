package logger

import (
	"fmt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
	"strings"
)

type logInterface interface {
	Info(msg string)
	Error(msg string)
	Print(v ...interface{})
}

type log struct {
	zapLog *zap.Logger
}

var (
	Log logInterface
)

func init() {
	logConfig := zap.Config{
		OutputPaths: []string{"stdout"},
		Level:       getLogLevel(),
		Encoding:    "json",
		EncoderConfig: zapcore.EncoderConfig{
			LevelKey:     "level",
			TimeKey:      "time",
			MessageKey:   "msg",
			EncodeTime:   zapcore.ISO8601TimeEncoder,
			EncodeLevel:  zapcore.LowercaseLevelEncoder,
			EncodeCaller: zapcore.ShortCallerEncoder,
		},
	}
	var err error
	zapLog, err := logConfig.Build()
	if err != nil {
		panic(err)
	}
	Log = &log{zapLog: zapLog}
}

func (l *log) Info(msg string) {
	l.zapLog.Info(msg)
}

func (l *log) Error(msg string) {
	l.zapLog.Error(msg)
}

func getLogLevel() zap.AtomicLevel {
	ll := strings.ToLower(os.Getenv("go.loglevel"))

	var zl zapcore.Level
	switch ll {
	case zap.DebugLevel.String():
		zl = zap.DebugLevel
	case zap.ErrorLevel.String():
		zl = zap.ErrorLevel
	case zap.WarnLevel.String():
		zl = zap.WarnLevel
	default: // Panic? DPanic? Fatal?
		zl = zap.InfoLevel
	}
	return zap.NewAtomicLevelAt(zl)
}

func (l *log) Print(v ...interface{}) {
	l.zapLog.Info(fmt.Sprint(v...))
}
